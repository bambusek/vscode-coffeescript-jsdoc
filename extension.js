const vscode = require('vscode');
const parser = require('./parser');

// Activate function
function activate(context) {
  console.log('Congratulations, your extension "coffeescript-jsdoc" is now active!');

  let disposable = vscode.commands.registerCommand('extension.createJSDocs', function () {
    // Get files's language
    const lang = vscode.window.activeTextEditor.document.languageId;
    if (lang == "coffeescript"){
      // Get actual line number
      const funcLine = vscode.window.activeTextEditor.selection.active.line;
      // Get the text on actual line
      const functionDefinition = vscode.window.activeTextEditor.document.lineAt(funcLine).text;
      // Parse the line and get the JSDoc template
      const jsDocs = parser.parse(functionDefinition);
      // Insert JSDoc into the file
      vscode.window.activeTextEditor.edit(function (editBuilder) {
        let pos = new vscode.Position(funcLine, 0);
        editBuilder.insert(pos, jsDocs);
      }).then(function () {
      });
    } else {
      vscode.window.showErrorMessage('This extension work only for coffeescript files!');
    }
  });
  context.subscriptions.push(disposable);
}
exports.activate = activate;

// Deactivate function
function deactivate() {
}
exports.deactivate = deactivate;