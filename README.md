# CoffeeScript JSDoc template generator

Easily create JSDoc templates for your coffeescript functions & instance/class variables.

## Issues & Contributions

Any kind of contribution or idea for further development is welcome. If you bump into any trouble, just create an issue at the [project's GitLab](https://gitlab.com/bambusek/vscode-coffeescript-jsdoc)

## Usage

Once you have your function or instance/class definition ready, just keep cursor on the same line and activate `create JSDoc` from the commands list.

### Basic usage for functions

```coffee
# Before
myFunction: (param1, param2) ->
  # Some code
  return

# After
###*
 * @param {*} param1
 * @param {*} param2
 * @return {*}
###
myFunction: (param1, param2) ->
  # Some code
  return
```

### Optional parameters

**optional parameters** are defined by `opt_` prefix

```coffee
# Before
myFunction: (param1, opt_param2) ->
  # Some code
  return

# After
###*
 * @param {*} param1
 * @param {*=} op_param2
 * @return {*}
###
myFunction: (param1, opt_param2) ->
  # Some code
  return
```

### Private functions

**private function** are defined by `_` suffix

```coffee
# Before
myPrivateFunction_: (param1, param2) ->
  # Some code
  return

# After
###*
 * @param {*} param1
 * @param {*} param2
 * @private
 * @return {*}
###
myPrivateFunction_: (param, param2) ->
  # Some code
  return
```

### Constructors

**constructor** is always named `constructor`

```coffee
# Before
constructor: (param1, param2) ->
  # Some code
  return

# After
###*
 * @param {*} param1
 * @param {*} param2
 * @constructor
###
constructor: (param, param2) ->
  # Some code
  return
```

### Class/Instance variables

**class/instance** variables are defined by `@` prefix

```coffee
class myClass
  # Before
  @classVariable = ...

  # After
  ###*
   * @type {*}
  ###
  @classVariable = ...

  ###*
   * @constructor
  ###
  constructor: ->
    # Before
    @instanceVariable = ...

    # After
    ###*
     * @type {*}
    ###
    @instanceVariable = ...
```

### Class/Instance private variables

**private class/instance** variables are defined by `@` prefix and `_` suffix

```coffee
class myClass
  # Before
  @classVariable_ = ...

  # After
  ###*
   * @type {*}
   * @private
  ###
  @classVariable_ = ...

  ###*
   * @constructor
  ###
  constructor: ->
    # Before
    @instanceVariable_ = ...

    # After
    ###*
     * @type {*}
     * @private
    ###
    @instanceVariable_ = ...
```

### Class/Instance private variables

**class/instance** constants are defined by `@` prefix and their name is all in uppercase (can be also private)

```coffee
class myClass
  # Before
  @CLASS_CONST = ...

  # After
  ###*
   * @const {*}
  ###
  @CLASS_CONST = ...

  ###*
   * @constructor
  ###
  constructor: ->
    # Before
    @INSTANCE_CONST_ = ...

    # After
    ###*
     * @const {*}
     * @private
    ###
    @INSTANCE_CONST_ = ...
```

### Indented definitions

JSDoc template will be always indented accordingly to the parent function/variable definition.

```coffee
# Before
  indentedFunction: (param1, param2) ->
    # Some code
    return

# After
  ###*
  * @param {*} param1
  * @param {*} param2
  * @constructor
  ###
  indentedFunction: (param, param2) ->
    # Some code
    return
```