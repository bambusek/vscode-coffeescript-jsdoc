/**
 * Parse line and create JSDoc template
 * @param {string} line 
 * @return {string}
 */
function parse(line){
  // Initialize JSDoc template
  var jsDocs = "";
  // Various regular expressions
  const funcRegExp = new RegExp(/^(\s*)([A-Za-z][\w.]*)\s*[:=](.*)->/);
  const paramsRegExp = new RegExp(/\s*\((\s*\w+\s*(,\s*\w+\s*)*)\)/);
  const optParamRegExp = new RegExp(/opt_[\w]*/);
  const privateRegExp = new RegExp(/[A-Za-z][\w]*_/);
  const classVariableRegExp = new RegExp(/^(\s*)@([A-Za-z][\w.]*)\s*[:=].*/);
  // Check weather the line is a function declaration
  if(funcRegExp.test(line)){
    // Get parts of the declaration
    const parts = funcRegExp.exec(line);
    var indentation = parts[1];
    if(indentation == undefined){
      indentation = '';
    }
    var funcName = parts[2];
    var funcParams = parts[3];
    // Begin JSDocs
    docsAddLine_(`###*`);
    // Check weather there are any parameters
    if(paramsRegExp.test(funcParams)){
      // Get string list of parameters
      const paramsParts = paramsRegExp.exec(funcParams);
      // Get actual array of truncated parameters
      const params = paramsParts[1].split(',').map(function(param){return param.trim()});
      // Add a JSDoc line for each param
      params.forEach(function(param){
        // Check if the parameter is optional
        if (optParamRegExp.test(param)){
          docsAddLine_(` * @param {*=} ${param}`);
        } else {
          docsAddLine_(` * @param {*} ${param}`);
        }
      });
    }
    // Check if the function is private
    if(privateRegExp.test(funcName)){
      docsAddLine_(` * @private`);
    }
    // Check if we are dealing with constructor
    if(funcName == 'constructor'){
      docsAddLine_(` * @constructor`);
    } else {
      docsAddLine_(` * @return {*}`);
    }
    // Finish JSDocs
    docsAddLine_(`###`);
  } else if (classVariableRegExp.test(line)){
    // Get parts of the declaration
    const parts = classVariableRegExp.exec(line)
    var indentation = parts[1];
    const varName = parts[2];
    // Begin JSDocs
    docsAddLine_(`###*`);
    // Add type/const JSDoc line
    if(varName === varName.toUpperCase()){
      docsAddLine_(` * @const {*}`);
    } else {
      docsAddLine_(` * @type {*}`);
    }
    // Check if the function is private
    if (privateRegExp.test(varName)) {
      docsAddLine_(` * @private`);
    }
    // Finish JSDocs
    docsAddLine_(`###`);
  }

  /**
   * Add line to JSDoc template
   * @param {string} string 
   * @param {boolean=} opt_end 
   * @private
   * @return {string}
   */
  function docsAddLine_(string, opt_end){
    jsDocs += indentation + string + '\n';
  }

  return jsDocs;
}
exports.parse = parse;